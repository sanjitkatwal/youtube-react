/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  View,
  Text,
} from 'react-native';

import Login from './components/Login';
import Home from './components/Home';

const App = () => {
  return (
    <View>
      <Login />
      <Home />
    </View>
  );
};


export default App;
